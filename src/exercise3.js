/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */

const fderive = (fn, h) => x => (fn(x+h) - fn(x-h))/(2*h);

const squear = x => x*x;

module.exports = {
  fderive,
  squear,
};

