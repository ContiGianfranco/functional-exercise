const faverage = numbers => {
	if(numbers==null) return 0;
	if(numbers.length == 0) return 0;
	return numbers.reduce((accumulator, currentValue) => accumulator + currentValue)/numbers.length;
};		

module.exports = faverage;	
