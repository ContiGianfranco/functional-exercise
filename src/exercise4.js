const wordCount = sequence => {
	if (sequence == null) return sequence;
	
	words = sequence.toLowerCase().match(/\w+/g);
  
	if (words == null || words.length == 0) return null;

	return words.reduce((acc, d) => {
				
		if (acc.hasOwnProperty(d)) {
			acc[d]++
		} else {
			acc[d]=1
		}
		return acc
	}, {});			
};

module.exports = {
  wordCount,
};
